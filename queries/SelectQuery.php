<?php

class SelectQuery extends AbstractQuery
{
    public function queryBegin()
    {
        $this->_query_begin = "SELECT";

        return $this;
    }

    public function joinQuery()
    {
        return $this->_query_begin . " " . $this->_column . " FROM " . $this->_table . ($this->_order_by ? " ORDER BY ".$this->_order_by : NULL) . ($this->_limit ? $this->_limit : NULL);
    }

    public function setColumns($column)
    {
        if (is_array($column)) {
            foreach($column as $key => $value) {
                $this->_column .= $value . ", ";
            }
            $this->_column = rtrim($this->_column, ", ");
        } else {
            $this->_column = $column;
        }

        return $this;
    }

    public function setOrderBy(array $params)
    {
        if (!is_array($params))
            throw new Exception('Неверный формат входных параметров в методе AbstractQuery::orderBy()');

        foreach($params as $key => $item) {
            $this->_order_by .= "{$key} $item, ";
        }
        $this->_order_by = rtrim($this->_order_by, ", ");

        return $this;
    }
}