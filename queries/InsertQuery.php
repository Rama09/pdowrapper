<?php

class InsertQuery extends AbstractQuery
{
    public function queryBegin()
    {
        $this->_query_begin = "INSERT INTO";

        return $this;
    }

    public function joinQuery()
    {
        return $this->_query_begin . " " . $this->_table . ($this->_limit ? $this->_limit : NULL);
    }
}