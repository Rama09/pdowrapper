<?php

abstract class AbstractQuery
{
    protected $_query_begin = "";

    protected $_table = "";

    protected $_column = "";

    protected $_order_by = "";

    protected $_limit = "";

    abstract public function queryBegin();

    abstract public function joinQuery();

    public function setTable($table)
    {
        if (is_array($table)) {
            foreach($table as $key => $value) {
                if (is_string($key))
                    $this->_table .= $value . " as ".$key . ", ";
                else
                    $this->_table .= $value . ", ";
            }
            $this->_table = rtrim($this->_table, ", ");
        } else {
            $this->_table = $table;
        }

        return $this;
    }

    public function setLimit($first, $second = null)
    {
        if ( !is_int($first) || (!is_int($second) && !is_null($second)) )
            throw new Exception('Неверный формат входных параметров в методе AbstractQuery::limit()');

        $this->_limit = " LIMIT {$first}" . (!is_null($second) ? ", {$second}" : NULL);

        return $this;
    }

}