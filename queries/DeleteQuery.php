<?php

class DeleteQuery extends AbstractQuery
{
    public function queryBegin()
    {
        $this->_query_begin = "DELETE FROM";

        return $this;
    }

    public function joinQuery()
    {
        return $this->_query_begin . " " . $this->_table . ($this->_limit ? $this->_limit : NULL);
    }
}