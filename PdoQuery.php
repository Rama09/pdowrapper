<?php

class PdoQuery
{
    private $_query;

    public function select()
    {
        $this->_query = new SelectQuery();
        $this->_query->queryBegin();

        return $this->_query;
    }

    public function delete()
    {
        $this->_query = new DeleteQuery();
        $this->_query->queryBegin();

        return $this->_query;
    }

    public function insert()
    {
        $this->_query = new InsertQuery();
        $this->_query->queryBegin();

        return $this->_query;
    }

    public function update()
    {
        $this->_query = new UpdateQuery();
        $this->_query->queryBegin();

        return $this->_query;
    }
}