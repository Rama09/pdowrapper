<?php

return array(
            'config' => array(
                'host'      => '127.0.0.1',
                'db'        => 'spphp2.loc',
                'user'      => 'root',
                'pass'      => '1234',
                'charset'   => 'utf8',
            ),
            'options' => array(
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES   => false,
            )
        );