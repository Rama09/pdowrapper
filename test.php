<?php

require_once __DIR__ . '/PdoQuery.php';
require_once __DIR__ . '/queries/AbstractQuery.php';
require_once __DIR__ . '/queries/SelectQuery.php';
require_once __DIR__ . '/queries/DeleteQuery.php';
/**
*** INSERT ***
 * $params = array('value1' => $value1, 'value2' => $value2);
 * $result = $pdo->insert('table', $params);
*
*** DELETE ***
 * $where = array('value1' => $value2, 'value2' => $value2);
* $result = $pdo->delete('table', $where);
*
*** UPDATE ***
 * $params = array('value1' => $value1, 'value2' => $value2);
* $where = array('value3' => $value3);
* $result = $pdo->update('table', $params, $where);
*/

//$pdo = new PDOWrapper();    (id != 38 AND id > 37) or klih = 37
//$result = $pdo->delete('news')->where(array('id' => array(38,37), 'kljh' => 37), 'or');
//var_dump($result);

$pdo = new PdoQuery();
$result = $pdo->select()
    ->setTable(array('t1' => 'news', 'article'))
    ->setColumns(array('t1.name', 't1.description', 'article.user_id'))->setOrderBy(array('t1.name' => 'DESC'))->setLimit(5,5);
//$result = $pdo->delete()->setTable(array('t1' => 'news', 'article'))->setLimit(3);
var_dump($result->joinQuery());