<?php

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 26.04.17
 * Time: 16:05
 */

/**
 * Class PDOWrapper
 *
 * $pdo = new PDOWrapper();
 *
 *** Прямой запрос ***
 * $result = $pdo->query('SELECT * FROM table WHERE value1 > :value1', array('value1' => $value1));
 *
 *** INSERT ***
 * $params = array('value1' => $value1, 'value2' => $value2);
 * $result = $pdo->insert('table', $params);
 *
 *** DELETE ***
 * $where = array('value1' => $value2, 'value2' => $value2);
 * $result = $pdo->delete('table', $where);
 *
 *** UPDATE ***
 * $params = array('value1' => $value1, 'value2' => $value2);
 * $where = array('value3' => $value3);
 * $result = $pdo->update('table', $params, $where);
 */
class PDOWrapper
{
    /**
     * @var PDO содержит объект PDO
     */
    private $_pdo;

    /**
     * @var array массив настроек подключения
     */
    private $_config;

    private $_params;

    private $_prepare_string;

    private $_where_string;

    private $_result;

    public function __construct()
    {
        $this->_config = require_once __DIR__ . '/pdo_config.php';
        $dsn = "mysql:host=".$this->_config['config']['host'].";dbname=".$this->_config['config']['db'].";charset=".$this->_config['config']['charset'];
        $this->_pdo = new PDO($dsn, $this->_config['config']['user'], $this->_config['config']['pass'], $this->_config['options']);
    }

    /**
     * @param string $table - имя таблицы
     * @param array $params - массив параметров array('column1' => value1, 'column2' => value2, ...)
     * @return $this
     */
    public function insert($table, array $params)
    {
        if(!isset($params[0])) {
            $temp = $params;
            unset($params);
            $params[0] = $temp;
        }

        $ready_params = $this->_prepareParams($params, 'insert');

        $this->_params = array();
        foreach($params as $key_item => $item) {
            foreach ($item as $key_value => $value) {
                $this->_params[$key_value.$key_item] = $value;
            }
        }

        $this->_prepare_string = "INSERT INTO {$table} ({$ready_params['insert_columns']}) VALUES {$ready_params['insert_masks']}";

        return $this;
    }

    /**
     * @param string $table - имя таблицы
     * @return $this
     */
    public function delete($table)
    {
        $this->_prepare_string = "DELETE FROM {$table}";

        return $this;
    }

    public function where($condition, $params)
    {
        if (is_string($condition)) {
            switch ($condition) {
                case 'and':
                    $prepare = $this->_prepareParams($params, 'where');
                    var_dump($prepare);die;
                    $this->_where_string = "WHERE " . implode(' AND ', $condition);
                    $this->_params = $params;
                    break;
                case 'or':
                    unset($condition[0]);
                    $this->_where_string = "WHERE " . implode(' OR ', $condition);
                    $this->_params = $params;
                    break;
            }

        }
        return $this;
    }


    /**
     * @param string $table - имя таблицы0
     * @param array $params - массив параметров для обновления
     * @param array $where - массив параметров для выражения where
     * @return $this
     */
    public function update($table, array $params, array $where)
    {
        $total_params = array(
            'params' => $params,
            'where_params' => $where,
        );

        $compare_params = array_intersect_ukey($total_params['where_params'], $total_params['params'], function ($key1, $key2) {
            if ($key1 == $key2)
                return 0;
            else if ($key1 > $key2)
                return 1;
            else
                return -1;
        });

        foreach ($compare_params as $key => $item) {
            $total_params['where_params']['where_match_'.$key] = $item;
            unset($total_params['where_params'][$key]);
        }

        $ready_params = $this->_prepareParams($total_params['params'], 'default');
        $ready_where = $this->_prepareParams($total_params['where_params'], 'where');


        $this->_prepare_string = "UPDATE {$table} SET {$ready_params} WHERE {$ready_where}";

        $this->_params = array_merge($total_params['params'], $total_params['where_params']);

        return $this;
    }

    /**
     * @param array $params - массив параметров
     * @param $flag - определяет формат выходных данных
     * @return array|bool|string
     * @throws Exception
     *
     * Подготавливает строки нужного формата для вставки параметров в подготовленный запрос
     */
    private function _prepareParams(array $params, $flag)
    {
        $result = false;
        switch ($flag) {
            case 'insert':
                $insert_columns = '';
                $insert_masks = '';

                $first_item = true;
                foreach($params as $item_key => $item) {
                    if (!is_array($item))
                        throw new Exception('Неверный формат входных параметров в методе PDOWrapper::insert()');
                    $first_value = true;

                    if($first_item === false)
                        $insert_masks .= ",";

                    foreach ($item as $key => $value) {
                        if ($first_value === true) {
                            $insert_columns = $key;
                            $insert_masks .= "(:{$key}{$item_key}";
                            $first_value = false;
                        } else {
                            $insert_columns .= ", {$key}";
                            $insert_masks .= ", :{$key}{$item_key})";
                        }
                    }

                    $first_item = false;
                }

                $result = array('insert_columns' => $insert_columns, 'insert_masks' => $insert_masks);
                break;
            case 'where':
                $insert_columns = '';
                $first = true;
                foreach($params as $key => $value) {
                    $key_replaced = str_replace('where_match_', '', $key);
                    if ($first === true) {
                        $insert_columns = $key_replaced . " = :" . $key;
                        $first = false;
                    } else
                        $insert_columns .= " AND ".$key_replaced." = :".$key;
                }
                $result = $insert_columns;
                break;
            case 'default':
                $insert_columns = '';
                $first = true;
                foreach($params as $key => $value) {
                    if ($first === true) {
                        $insert_columns = $key . " = :" . $key;
                        $first = false;
                    } else
                        $insert_columns .= ", ".$key." = :".$key;
                }
                $result = $insert_columns;
                break;
        }

        return $result;
    }

    /**
     * @return bool|int Возвратит false если запрос не выполнился
     *
     * Выполнение запроса
     */
    public function execute()
    {
        $result = $this->_pdo->prepare($this->_prepare_string);
        if ($result->execute($this->_params))
            return $result->rowCount();

        return false;
    }

    /**
     * @param array $count
     * @return $this
     * @throws Exception
     *
     * Выставляет лимит
     */
    public function limit(array $count)
    {
        if ( !is_int($count[0]) && (isset($count[1]) && !is_int($count[0])) )
            throw new Exception('Неверный формат входных параметров в методе PDOWrapper::limit()');

        $this->_prepare_string .= " LIMIT {$count[0]}" . (isset($count[1]) ? ", {$count[1]}" : NULL);

        return $this;
    }

    /**
     * @param $string
     * @return $this
     *
     * Выставляет сортировку
     */
    public function orderBy($string)
    {
        $this->_prepare_string .= " ORDER BY {$string}";

        return $this;
    }

}